# News and Updates - April 2020 onwards

## Exam details (18 May 2020)

Dear Students,

The faculty has now scheduled the Math Methods exam.

The exam will open at 10am (BST) on Monday 1 June and will be open for 48 hours,
closing at 10am on Wednesday 3 June.

There will be a PDF to download with 20 multiple-choice questions, each of which
has exactly one right answer. You send your answers to blackboard.

In case of technical problems on the day, you must e-mail the school office
(coms-info@bristol.ac.uk), not me.

Drop-ins will continue on Discord every Thursday before the exam, that is on 21
and 28 May.

Good luck!

Greetings,
David

## Update (7 May 2020)

Dear Students,

Exam update: we have slightly changed the plan for the exam, so you will now
have unlimited attempts in a 48-hour period (instead of 24). This should make it
easier for students in different time zones to engage. The date for the exam
will be set by the Faculty, who will try and timetable everything in a way that
doesn't cause any clashes - first-year CS won't be a problem, it's later years
with optional units and joint honours programmes that are currently the sticking
point.

Today, from 11am-1pm and 3pm-4pm, I will be on the Math Methods Discord server
to help anyone who has questions - please use this opportunity if you're not
sure about anything. You can join from your browser if you don't want to
download any software, and you can join without making an account or giving an
e-mail address if you want to as well. The link is in a previous announcement
so that it's not completely public.

Next week is revision week, during which there will be no new material or
exercise sheets. There will be another drop-in on Thursday the 14th at the usual
times.

The week after that is 'Teaching Consolidation Week'. I'm not sure what that
means either so I'm treating it as another revision week, which means I'll be on
Discord on Thursday the 21st as well. The exam won't be until June so that gives
you several more opportunities to ask me questions, and you have previous years'
exams to practice on.

Hope to see many of you today.

Greetings,

David


## Update (1 May 2020)

Dear Students,

The drop-in session on Discord this Thursday went well, a couple of students
asked questions and helping them with a combination of voice and text worked
surprisingly well.

Me and some TAs will now be holding further Discord drop-ins on Thursday 7 May
and Thursday 14 May, on both days from 11am-1pm and 3pm-4pm UK time. Please do
join and ask if you have any questions; at other times the online forum is still
around for you.

It looks at the moment like the exam will be scheduled in June by the exams
office and there will be one 24-hour period from midnight to midnight UK time
in which you will be able to take it. Since the exam was originally designed to
be 2 hours long, I hope this works for everybody even if you are not in the UK.
If for any reason you cannot or do not want to take the exam at the date that I
hope you will be told shortly, you should complete the 
[deferral form](https://www.bristol.ac.uk/students/academic-advice/extenuating-circumstances/)
before the exam date (which I hope we will all know soon).
As always, I will update you as soon as I get more information.

I have put up the solutions for week 22's exercises, which leaves us one more
week of content in which I'd like you to read the rest of the differential
equations notes, watch videos 4-6 on the topic and then do the second
differential equations worksheet.

I have also created a bonus video 
[differential equations 7](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4235595_1&course_id=_237269_1)
(blackboard login required as usual) that is not
assessed but that you may find interesting, it explains the logistic model for
the spread of a virus in a population and "the curve" which we're all trying
to flatten by staying at home and working hard on our mathematics, or something
like that.

Greetings,

David

## Differential Equations (27 April 2020)

Dear students,

This week we are starting on our last topic that stretches over two weeks:
differential equations. Differential equations are the mathematical foundation
for science, and you might encounter them later on directly in units such as
Computational Neuroscience in year 3. If you are doing High-Performance
Computing (HPC) later on, one of the things that HPC is useful for is simulating
the solution to a differential equation for something like the flow of air
across an aeroplane wing - indeed that's how aeroplanes and many other things
these days are designed, you test a model on a computer before you start 
building the real thing.

Differential Equations also appear when you try and model the effect of a
pandemic on a population, and the solution to this particular equation is
exponential growth - at least in the initial stages. I've talked about that in
the video for this week. Of course, the number of infected people can't go on
growing exponentially forever in a finite population, and eventually the growth
transitions to something called a logistic model. One of the big questions at
the moment is where in this process the different countries in the world are at
the moment. If I find the time I might talk about that too a bit, but it won't
be assessed if I do. Next time anyone asks me what all this maths stuff is good
for, that will be my answer.

For this week, you should read the lecture notes up to page 5 before the section
"Inhomogeneous Equations", and watch the three linked videos on the contents
page which cover the same material. I apologise in advance for the quality of
the audio, I will see if I can improve this for next week with the hardware that
I have at home. I have just updated the lecture notes with a version that fixes
a few typos so you should download them again if you've already got the old
version.

After this, you should attempt this week's exercise sheet and ask any questions
you may have in the online forum. I have posted the solutions to last week's
exercises and there will be solutions to this week's ones too by next Monday.

Greetings,
David

## The rest of term (21 April 2020)

Dear students,

Here is some information about how Math Methods will continue.

**Term schedule**

As you might have heard, the university has changed the term as follows:

  - You have four weeks of teaching after Easter, from Monday 20 April to Friday 15 May.
    These weeks are called "new week 21" up to "new week 24" in the university numbering system.
  - You then have two weeks of revision time, from 18 May to 29 May.
  - After this, there is a three week assessment period from 1-19 June.

You do not have to be in Bristol for any of this time, even if you hold a Tier 4
student visa. In fact, if you are not currently in Bristol then you should not
attempt to return for the time being.

**Assessment**

First-year Computer Science students will take two formative assessments this 
summer, in Math Methods and Object-Oriented programming. You should ask the
school office (coms-info@bristol.ac.uk) for information on any other units, my
current understanding is that the assessment in these units (e.g. Theory of
Computation) is cancelled.

For Math Methods, the current plan is that you will sit a 20-question online
multiple choice exam. You need to get 40% (8 questions correct) to pass, but
your transcript will just show "pass" and not a mark. You will also be able to
use any materials you like in this exam and you will get unlimited attempts
during a fixed time period; after each attempt you will see how many questions
you got right. This assessment will take place during the three-week June
assessment period and I will tell you more details as soon as I know them.

**Unit schedule**

The remaining teaching in this unit will be online and asynchronous. This means
that I will provide you with materials for each week, and you will work on these
at a time of your choosing. There will be no fixed hours during the day when you
are expected to be online; this is to be inclusive of students in different time
zones and with degraded internet connections.

Each week in the remaining four weeks of teaching, you will get lecture notes to
read, videos to watch and an exercise sheet to solve by yourselves; I will upload
sample solutions to the exercises in the following week. The notes and videos
cover the same material, so you can choose the version you prefer (or use both).

The exercise sheets are the really important part - you learn mathematics by
doing mathematics, so please try and solve all the exercises each week.

All materials will be linked to on the [contents page](contents.md) as usual.

For the current week (20-24 April), the topic is Taylor series. You have a
document with lecture notes, a video and an exercise sheet. Please excuse
the poor sound quality of the video, I made it before Easter with the built-in
microphone on my laptop. The university has now provided me with a headset with
a better microphone, so the quality of future videos will hopefully improve.
The lecture notes are luckily not affected my microphone quality.

**Getting help**

For support, you will use the online forum that I have set up on gitlab - please
see the [getting help page](help.md) for details. I would like to encourage you
to make use of this forum as much as possible, and me and the TAs will try our
best to answer your questions. I know this is less effective than having an
actual exercise class, and I know this is not what you signed up for when you
joined the university, but the university buildings are closed to reduce the
number of deaths as a result of Covid-19 and we have to respect that. I also do
not have the infrastructure at home to provide effective and reliable "real time"
support to a large cohort of students, so I am asking everyone to accept that
your support from now on will be through the online forum.

I also ask you to please not use the "private" option on the online forum.
It is not fair if I answer a question for only one student and the other
students cannot see the same answer. Since the forum is hosted on gitlab,
you can register a free account under any username you like, so you can stay
anonymous by picking a username that has nothing to do with your real name, if
you prefer that.

Greetings,
David