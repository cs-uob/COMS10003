# Assessment

_The following is an update based on the current COVID-19 mitigation plan as of 1 April 2020._

The following is our current plan for teaching and assessing Mathematical Methods from Easter 2020 onwards. We cannot promise that this plan won't change between now and the end of term, but we will update you as soon as possible if it does.

I recommend that you prepare for the rest of the unit and the assessment based on the current plan, until you hear otherwise from me or the university.

## Teaching

Teaching will continue online after Easter; as term has been extended we will cover all the material that was originally planned. Your work on this unit will include:

  * Reading the lecture notes that I provide.
  * Watching short videos that I provide.
  * Optionally, reading or watching further material online where I provide links to it.
  * Solving the problem sheets by yourself, and then checking your solutions against the sample solutions when they are released.
  * Working on and revising the material in your own time.
  * If something is not clear, asking questions on the online forum as explained on the [help page](help.html).

There will be no more in-person workshops, so your main source of support will be the online forum. Please use this as much as you need, I know this is a less good option than talking to a TA in person but we don't have that option anymore and there's nothing I can do about it.

## Exam

The current plan is to hold an online exam for Math Methods, with 20 multiple-choice questions. This will be the same exam that you would have sat in the exam hall, but now we will give you a PDF document and you will select an answer from A-E for each question on an online form and submit that.

We currently plan to run the exam as follows.

  1. You will still complete the exam in a fixed time period, but this will be longer than the original 2 hours. I will tell you as soon as I know more about this - the timing is still being worked out by the exams office.
  2. You can use any notes and textbooks (whether in paper or PDF format) that you like. The exam is effectively "open book". You can still use a calculator.
  3. You must get 40% (8 of 20 questions correct) to pass the exam.
  4. You will get an unlimited number of attempts at the exam within a fixed time period. An attempt means you submit your answers to all questions, and you will get back a number telling you how many questions you got right.
  5. When you have passed the exam, you will simply get a "pass" for this unit on your transcript. The current plan is that you will not get a grade on your transcript.
  6. After the exam has closed for everyone, I will release the sample solutions.

## Past exams

You need to be logged in to blackboard for these to work.

The Math Methods material in past years was not always the same - some years we did not do differential equations; until last year there was a section on Number Theory which you can ignore this year.

  * [Class test from week 3](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-test-WS.pdf) and [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-test-SOL.pdf).
  * [January mock exam, with solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/10003-20J-answers.pdf)
  * [Summer 2019 exam, with solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/10003-19-answers.pdf)
  * The 2018 exam seems to have been lost.
  * [Summer 2017 exam, with solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/COMS10003-17-ans.pdf)
  * [Summer 2016 exam, with solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/COMS10003-16-ans.pdf)
  * [Summer 2015 exam, with solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/COMS10003-15-ans.pdf)

You can also practice with the [online self-tests](https://www.ole.bris.ac.uk/webapps/blackboard/content/listContent.jsp?course_id=_237269_1&content_id=_4020328_1) in blackboard.

## Progressing to second year

The main purpose of teaching you mathematics in first year is to prepare you for units in later years: the new units Algorithms 2 and Programming Languages and Computation in year 2 and for example the options Computational Neuroscience, Machine Learning, Computer Graphics, Cryptology, Image Processing, Types and Lambda Calculus all involve mathematics in some form or other. 

A side-effect of assessing Math Methods in first year is that in a normal year we fail a small number of students whose exam performance suggests they are definitely not ready to progress into second year. Students who just about scrape a pass in Math Methods are still much more likely to struggle in later years than students who do well in the exam.

This year, as you will have noticed, there are a number of ways for every student to get a pass in Math Methods even if they don't know the material. Possibly, we will see an increased rate of students failing their second year as a result if we can assess it properly again.

I suggest that you still try and take the exam under as close to "exam conditions" as possible. If you do not manage to get 50% (10 of 20 questions right) on your first attempt, in no more than three hours of work, then you should consider yourself at an increased risk of failing the degree in future years and you should take steps to mitigate this risk. These could include:

  * Based on your performance across the whole first year, spend some time thinking about whether this degree is right for you.
  * Spend extra time revising mathematics over the summer.
  * Talking to your personal tutor (via video link or phone call, for the time being).

## Old information

_This was the previous assessment information. It is now out of date._

This unit is worth 20 CP (credit points), or 1/6 of your first year at university.

Your mark will be determined 100% from a final multiple-choice exam in the
May/June 2020 exam period.

To help you prepare for this exam, you will have the following mandatory events:

  * A workshop, every teaching week except week 3.
  * An in-class test in the week 3 workshop hours. This will give you an early
    indication of how well you are working compared to the standards expected
    of you at university.
  * A mock exam in the January 2020 exam period. This covers material from
    teaching block 1 only and is intended as a 'dry run' for the summer exam.

In addition, the following optional materials are available. They are hosted
on the university's blackboard system so they are only available to current
Bristol students. If you log in to [blackboard](https://ole.bris.ac.uk) and
then come back here, the links should work.

Note that the content of the unit changes a bit every year. For example, in
previous years weeks 12 and 24 were not revision weeks, so there was extra
material on number theory. Up until 2017, the exams were bookwork rather than
multiple choice. Past exams therefore do not reflect the currently taught
material or exam format exactly.

You need to be [logged into blackboard](https://www.ole.bris.ac.uk) to use the following links.


  * [Class test from week 3](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-test-WS.pdf) and [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-test-SOL.pdf).

We have created some non-assessed [online self-tests](https://www.ole.bris.ac.uk/webapps/blackboard/content/listContent.jsp?course_id=_237269_1&content_id=_4020328_1)
in blackboard to help you practice. You will need to be logged in to blackboard for this link to work.

## Calculators

You may bring a calculator to the mock and real exams as long as it meets
the Faculty of Engineering guidelines and has the Faculty Seal of Approval.

The faculty has a [list of approved calculator models](http://www.bristol.ac.uk/engineering/current-students/taught-programme-handbook/#9). You must present
your calculator to the faculty office (Queens Building, room 1.43) to get the
seal of approval and you should do this before the end of your first teaching
block.

Calculators must be one of the exact model numbers listed in the link above.
Generally, these are calculators with scientific and trigonometric functions,
limited symbolic algebra (they can display roots, fractions, logarithms etc.)
but these calculators cannot do integrals or derivatives, full symbolic
algebra such as solving equations or equation systems etc.