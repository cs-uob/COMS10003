# COMS10003 Math Methods

This is the repository and website for the COMS10003 'Mathematical Methods for Computer Science' unit at the University of Bristol for the 2019-20 academic year.

If you are browsing the repository, you can find the website at https://cs-uob.gitlab.io/COMS10003.

See the menu on the left for links to different pages.

<div style="color: #fff; background-color: #000; padding: 6px">
<h1>Coronavirus update</h1>
</div>

Effective immediately, Math Methods is moving to an online delivery method. This means that all lectures, workshops and drop-ins from week 20 onwards no longer take place physically.

I will update everyone with the arrangements for the exam as soon as I know what's happening - it is likely that the summer exam period will be disrupted.

Further information can be found on the [news page](news.md).

From now on, this unit will continue as follows:

  * Instead of going to lectures, you read the prepared lecture notes.
  * I will provide further links to online resources to help you understand the material. (All the material we do in Analysis is part of a standard curriculum and there are lots of good resources online).
  * Instead of coming to workshops, you do the weekly sheets in your own time (alone or with friends, however you prefer) and check your results against the solutions when they are released.
  * The role of myself and the TAs will be to support you through the [online gitlab forum](https://gitlab.com/cs-uob/COMS10003/issues?scope=all&state=all).
  * If the university provides suitable support and equipment, I will look into creating videos - some combination of video lectures and individually answering students' questions.
