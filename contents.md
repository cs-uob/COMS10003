# Unit contents

In 2019-20, Mathematical Methods will focus on three main topics:

  * Logic and Proof (weeks 1-7)
  * Linear Algebra (weeks 9-16)
  * Analysis (weeks 17, 19-23)

This unit will be taught by three lecturers, each of them taking one of the
main topics. Each of us has a slightly different lecturing style; you might
need to adapt your note-taking or revision methods to the different styles
and topics in this unit.

# Unit materials

  * [introduction slides](https://cs-uob.gitlab.io/COMS10003/files/intro-coms10003.pdf)

Our lectures are recorded via a university system called replay.
However, replay only recordes the video feed from the projector and the
audio from the microphone - it does not have a camera that records anything
the lecturer writes on the blackboard, which will happen often as this
is a mathematics unit.

To watch lectures again,

  * log in to [blackboard](https://ole.bris.ac.uk)
  * then you can use this link to [replay](https://www.ole.bris.ac.uk/webapps/blackboard/content/launchLink.jsp?course_id=_237269_1&tool_id=_4152_1&tool_type=TOOL&mode=view&mode=reset)

### Logic and Proof

This part of the unit is taught with slide-based lectures. The materials 
for logic are available in three versions: slides (exactly what you saw on screen),
handout (some 'animations' over several slides reduced into one), and
print (handouts with 6 slides per page).

  * Week 1:
    [slides](https://cs-uob.gitlab.io/COMS10003/files/logic-1-SLIDES.pdf),
    [handout](https://cs-uob.gitlab.io/COMS10003/files/logic-1-HANDOUT.pdf),
    [print](https://cs-uob.gitlab.io/COMS10003/files/logic-1-PRINT.pdf)

  * Week 2:
    [slides](https://cs-uob.gitlab.io/COMS10003/files/logic-2-SLIDES.pdf),
    [handout](https://cs-uob.gitlab.io/COMS10003/files/logic-2-HANDOUT.pdf),
    [print](https://cs-uob.gitlab.io/COMS10003/files/logic-2-PRINT.pdf)

  * Week 3:
    [handout](https://cs-uob.gitlab.io/COMS10003/files/proof-1.pdf)

  * Week 4:
    [handout](https://cs-uob.gitlab.io/COMS10003/files/proof-2.pdf)
    [inductive proof example](https://cs-uob.gitlab.io/COMS10003/files/example_inductive_proof.pdf)

  * Week 5:
    [handout](https://cs-uob.gitlab.io/COMS10003/files/proof-3.pdf)

  * Week 6:
    [slides](https://cs-uob.gitlab.io/COMS10003/files/logic-3-SLIDES.pdf),
    [handout](https://cs-uob.gitlab.io/COMS10003/files/logic-3-HANDOUT.pdf),
    [print](https://cs-uob.gitlab.io/COMS10003/files/logic-3-PRINT.pdf)

  * Week 7:
    [slides](https://cs-uob.gitlab.io/COMS10003/files/logic-4-SLIDES.pdf),
    [handout](https://cs-uob.gitlab.io/COMS10003/files/logic-4-HANDOUT.pdf),
    [print](https://cs-uob.gitlab.io/COMS10003/files/logic-4-PRINT.pdf)

### Linear Algebra

In this part of the unit, the slides are supplemented by separate lecture notes.

  * Week 9: vector spaces and the dot product
    [slides](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecs-lec-6up.pdf)
    [notes](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecs-notes.pdf)

  * Weeks 10 and 11: vector spaces, span and basis
    [slides](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecspaces-lec-6up.pdf)
    [notes](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecspaces-notes.pdf)

Week 12 is a revision week - no new material.

  * Week 13 (January): matrices
    [slides](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-mats-lec-6up.pdf)
    [notes](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-mats-notes.pdf)

  * Week 14: inverting matrices
    [slides](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-invm-lec-6up.pdf)
    [notes](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-invm-notes.pdf)

  * Weeks 15 and 16: eigenvectors
    [slides 1](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-eig-lec-1-6up.pdf)
    [slides 2](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-eig-lec-2-6up.pdf)
    [notes](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-eig-notes.pdf)

### Analysis

In this part of the unit, we will be working largely on the blackboard so there are no slides.
However, printed lectures notes are available.

  * Week 17: complex numbers
    [lecture notes](https://cs-uob.gitlab.io/COMS10003/files/Complex-notes.pdf)
  * Week 19: differentiating in one dimension
    [lecture notes](https://cs-uob.gitlab.io/COMS10003/files/analysis-1-notes.pdf)
  * Week 20: differentiating in multiple dimensions
    [lecture notes](https://cs-uob.gitlab.io/COMS10003/files/analysis-2-notes.pdf).
    If you want further material on this topic, have a look at 
    [Khan academy on partial derivatives](https://www.khanacademy.org/math/multivariable-calculus/multivariable-derivatives/partial-derivative-and-gradient-articles/a/introduction-to-partial-derivatives),
    [Optimizing multivariable functions](https://www.khanacademy.org/math/multivariable-calculus/applications-of-multivariable-derivatives/optimizing-multivariable-functions/a/maximums-minimums-and-saddle-points) and
    the [Hessian matrix](https://www.khanacademy.org/math/multivariable-calculus/applications-of-multivariable-derivatives/quadratic-approximations/a/the-hessian).

  * Week 21 (now from 21 April onwards): Taylor series. [lecture notes](https://cs-uob.gitlab.io/COMS10003/files/analysis-3-notes.pdf) and [video](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4201945_1&course_id=_237269_1). You must be logged in to blackboard to see the video.
  * Week 22: introduction to differential equations. [lecture notes for weeks 22-23](https://cs-uob.gitlab.io/COMS10003/files/diffeq-notes.pdf) and videos: 
    [part 1](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4208620_1&course_id=_237269_1), [part 2](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4208622_1&course_id=_237269_1) and [part 3](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4224770_1&course_id=_237269_1). 
  * Week 23: Please read the rest of the lecture notes linked in week 22. Videos:
    [part 4](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4231160_1&course_id=_237269_1),
    [part 5](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4231162_1&course_id=_237269_1) and
    [part 6](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4231163_1&course_id=_237269_1).
  * Bonus content (not assessed): [the logistic equation, or flattening the curve](https://www.ole.bris.ac.uk/webapps/sofo-mediasite-content-bb_bb60/Launcher.jsp?content_id=_4235595_1&course_id=_237269_1)

Textbook publisher Springer has temporarily made [Analysis for Computer Scientists](https://link.springer.com/book/10.1007/978-3-319-91155-7) free to download for a while during Covid-19.
This book covers everything we do in analysis in this unit and a lot more; if you are particularly interested in the topic or want to read up on something again that you don't quite understand yet then please make use of this extra resource.

# Worksheets and solutions

The worksheets for the workshops are here.

### Logic and Proof

  * Week 1: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/logic-1-WS.pdf)
  * Week 2: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/logic-2-WS.pdf)
  * Week 3: no worksheet - in-class test
  * Week 4: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/proof-1-WS.pdf)
  * Week 5: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/proof-2-WS.pdf)
  * Week 6: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/logic-3-WS.pdf)
  * Week 7: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/logic-and-proof-WS.pdf)

### Linear Algebra

  * Week 9: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecs-wsheet.pdf)
  * Week 10: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecspaces-wsheet1.pdf)
  * Week 11: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-vecspaces-wsheet2.pdf)

Week 12 is revision week (no new material), followed by Christmas break and January exams.

  * Week 13: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-mats-wsheet.pdf)
  * Week 14: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-invm-wsheet.pdf)
  * Week 15: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-eig-wsheet-1.pdf)
  * Week 16: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/mmcs-adc-eig-wsheet-2.pdf)

### Analysis

  * Week 17: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/Complex-worksheet.pdf)
  * Week 19: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/analysis-1-worksheet.pdf)
  * Week 20: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/analysis-2-worksheet.pdf)
  * Week 21: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/analysis-3-worksheet.pdf)
  * Week 22: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/diffeq-worksheet-1.pdf)
  * Week 23: [worksheet](https://cs-uob.gitlab.io/COMS10003/files/diffeq-worksheet-2.pdf)

## Solutions

Links to the solutions will appear here after the workshops.

Solution sheets are hosted on blackboard and are only accessible to students
taking the unit. Please [log in to blackboard](https://ole.bris.ac.uk) before
using the links below.

### Logic and Proof

  * Week 1: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-1-SOL.pdf)
  * Week 2: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-2-SOL.pdf)
  * Week 3: class test, no worksheet
  * Week 4: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/proof-1-SOL.pdf)
  * Week 5: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/proof-2-SOL.pdf)
  * Week 6: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-3-SOL.pdf)
  * Week 7: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/logic-and-proof-SOL.pdf)

### Linear Algebra

  * Week 9: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-vecs-wsheet-ans.pdf)
  * Week 10: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-vecspaces-wsheet1-ans.pdf)
  * Week 11: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-vecspaces-wsheet2-ans.pdf)
  * Week 13: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-mats-wsheet-ans.pdf)
  * Week 14: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-invm-wsheet-ans.pdf)
  * Week 15: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-eig-wsheet-1-ans.pdf)
  * Week 16: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/mmcs-adc-eig-wsheet-2-ans.pdf)

### Analysis

  * Week 17: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/Complex-worksheet-solutions.pdf)
  * Week 19: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/Analysis-1-solutions.pdf)
  * Week 20: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/Analysis-2-solutions.pdf)
  * Week 21: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/Analysis-3-solutions.pdf)
  * Week 22: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/diffeq-worksheet-1-solutions.pdf)
  * Week 23: [solutions](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/diffeq-worksheet-2-solutions.pdf)

# January Mock Exam

The January mock exam, with solutions, can be found at [this link](https://www.ole.bris.ac.uk/bbcswebdav/courses/COMS10003_2019_TB-4/solutions/10003-20J-answers.pdf) (university login required).