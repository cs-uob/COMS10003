# Prerequisites

The entry requirements for Computer Science at Bristol are normally
an A* grade in A-level maths, and English language skills at level B2
in the Cambridge languages framework. The A-level in Further Maths is
appreciated but not required.

This unit assumes that you have mathematics knowledge to the level of
an A* in A-level mathematics, or equivalent (e.g. international
baccalaureate).

The following self-test questions are based on a document kindly provided
by our school of mathematics. If you can not answer most of them easily
when you start term, you are encouraged to spend some extra time revising
your A-level mathematics (possibly buy a textbook for this) and to speak
to the unit staff. There are no sample solutions: this is material that
you are already expected to know.

  1. Solve the following quadratic equations:
    * $$X^2 - 8X + 15 = 0$$
    * $$2X^2 + 3X + 1/2 = 0$$
    * $$X^2 - 2X + 3 = 0$$
    * $$X^2 - X + 1/4 = 0$$

  2. Divide $$6X^5 + 49X^4 + 44X^3 - 298X^2 - 131X + 420$$ by $$2X+3$$.

  3. Expand $$(x + 2/y)^5$$ using binomial expansion.

  4. Solve the equation $$2 = 5^x$$ and give both an exact (symbolic)
     solution and a numerical answer to 4 digits after the decimal point.

  5. Differentiate the following:
    * $$3X^5 + 9X^3 - 4X^2 + 7X - 1$$
    * $$(X^2-1)(3X-5)$$
    * $$2^{X-1}$$
    * $$ \sin(X^2-5)$$

  6. Find the sum of the following:
    * All numbers from 1 to 2019.
    * All even numbers from 8 to 128.
    * The value of $$3n+1$$ for $$n$$ from $$1$$ to $$75$$.
    * $$2^n$$ for n from $$3$$ to $$23$$.

  7. Find the local minima and maxima of $$X^3 - 7X^2 + 1$$.

  8. Find the indefinite integrals of the following functions.
    * $$X^7 - 9X^3 + X$$
    * $$2\sin(X) + 5 \cos(X)$$
    * $$\sin(X + c)$$ where $$c$$ is a constant.
    * $$\sin(2X)\cos(X)$$.
    * $$1/(X^2 + 1)$$.

  9. Find the length of the vector $$(4, 3)$$ in the 2-dimensional plane
     and the angle between this vector and the vector $$(1, 1)$$.

  10. Decompose the following into partial fractions:

$$
\frac{X^2 + X - 2}{(X+1)(X^2-3)}
$$