# Schedule

**Please see the [news page](news.md) for updates from April 2020 onwards.**

COMS10003 Mathematical Methods for Computer Scientists is a unit worth 20 credit 
points that runs across the entire first academic year (TB4, in Bristol terminology).

Every week during teaching weeks, you will have 

  * One lecture, Mondays from 11-12 in Queens Building 1.40 'Pugsley'.
  * A mandatory 2-hour workshop, Thursdays from 11-1 in MVB 1.11.
  * An optional drop-in session in the MVB atrium, Thursdays from 4-5pm.

At the workshops, you will need to sign in so that we can take a register
of attendance. Starting in the 2019-20 academic year we will be stricter
about following up when students fail to attend workshops.

## The academic year

The Bristol academic year has two teaching blocks of 12 weeks each. However,
only 10 weeks per teaching block are teaching weeks:

  * In teaching block 1 (before Christmas), weeks 1-7 and 9-11 are
    teaching weeks. Week 8 is Explore Week and week 12 is revision week.
  * In teaching block 2 (after Christmas), weeks 13-17 and 19-23 are
    teaching weeks. Week 18 is Explore Week and week 24 is revision week.

During the Explore Weeks (8 and 18), there are no lectures or workshops
on this unit. During the revision weeks (12 and 24), the lectures and
workshops turn into extra drop-in sessions: there will be no new material,
but you can turn up and ask us questions.

There is also a 3-week break in teaching block 2 for Easter between weeks
21 and 22 in 2020.

# Time management

At university, you are expected to manage your own time and you
should talk to your personal tutor if you have any questions about
time management or anything else about university.

As a very rough guide, you should expect to spend at least 40 hours per teaching
week on university work. Since this unit is 1/6 of your credit points in each
term, this means that the average student should aim to spend at least 6-7 hours
per teaching week on this unit. In other words, on top of the 3-4 hours of contact time
each teaching week (1h lecture, 2h workshop, 1h optional drop-in) you should on average
spend another 3-4 hours each week of your own time on this unit.

During this time, you can work on your notes, revise material, look up
content that you don't understand yet, solve exercises and discuss the unit
content with your fellow students.

Of course, the time you need for each unit will vary between students.
If you have for example taken Further Maths and find the content in this unit
really easy one week, then by all means spend more of your time on other units.
