# Getting help

We are expecting up to 200 students on this unit in 2019-20.

To provide you with the best support possible under these
circumstances, the following help is available:

  * You can ask questions during a lecture. There will usually
    be a few minutes at the end of each lecture especially for
    questions.
  * You can ask the TAs (Teaching Assistants) who will be in
    the 2-hour workshops. They are there specifically to help
    you: please make use of this service.
  * TAs will also be in the atrium during the drop-in session
    on Thursday afternoon. Turn up and ask your questions any
    time during this hour.
  * You can ask questions online (see below).

You are also encouraged to discuss the unit material with your
fellow students on the unit. You can share your solutions to
the workshop exercises with them - since this unit does not
have any assessed coursework, there is no danger of plagiarism.

The drop-in session for this unit in the atrium takes the place
that office hours might take on some other units. This is so that
we can have more than one person present in the drop-in session
to answer your questions more quickly.

Outside of the contact hours, we ask for your understanding that

  * You can not turn up at our office any time you like and get
    an immediate private lesson. With one lecturer at a time and
    almost 200 students, this does not scale. Use the drop-in
    session and workshops for getting help in person, or ask your
    questions online.
  * You can not e-mail us with questions about the unit: you should
    use the online system instead (see below). If you have a
    question, it is safe to assume that at least 20 other students
    are asking themselves the same thing. By posting your question
    online, we can reply once and then everyone in the cohort can
    see the same answer, which is both efficient and fair.

## Getting help online

The university has bought an online learning environment called
'Blackboard'. Its user interface is less than ideal for computer
science students at the best of times, but it is all but useless
for mathematics - the 'formula editor' looks like something from
the 1990s and takes much longer to use than it should.

Instead, we will be using Gitlab's issues feature for online
discussion in this unit. This has the following advantages:

  * Modern and fast user interface.
  * Optional anonymity (you can choose your own username).
  * You can write mathematical formulae in MathJax/TeX syntax,
    the standard across mathematics, computing and engineering
    disciplines.

[Ask your questions here](https://gitlab.com/cs-uob/COMS10003/issues?scope=all&state=all)
or read on for more detailed instructions.

## Signing up for Gitlab

To ask a question, you first need to create an account on
https://gitlab.com. This is free and does not require a
phone number or credit card.

  * You need an e-mail address to sign up. This can be any
    e-mail address of yours that you like, it does not have
    to be your university one (and is completely irrelevant
    for asking questions online anyway).
  * You can pick any username that you like that is not
    already taken. It does not have to be related to your
    real name or university username - if you want to be
    anonymous, you can do this. We will treat all questions
    equally.
  * **You must not re-use your university password, or any
    password based on your university password, for any
    service outside the university. This includes gitlab.**

## Asking questions

We are using Gitlab's 'issues board' for questions. This is actually a
tool to track bugs in code, but it is useful for discussions too.

To ask a question, go to 
https://gitlab.com/cs-uob/COMS10003/issues?scope=all&state=all
and select "New issue" in the top right corner.
You will be asked to log in if you're not logged in already.

Give your question a short title and ask your question in the 'write' box.
The editor supports (Gitlab flavoured) markdown, if you know what that is
(or click the link below the editor to open a description).

We will tag questions with labels such as 'General' or different parts
of the unit such as Logic and Proof, Linear Algebra or Analysis.
You can browse all questions sorted by label at
https://gitlab.com/cs-uob/COMS10003/-/boards

## Writing mathematics in questions

*You do not have to use this feature - if it seems complicated to you,
just type your question normally and we will answer it just like any
other question.*

Gitlab supports MathJax/TeX syntax. You can write a formula inline like this:

```
Pythagoras' formula is $`x^2 + y^2 = z^2`$ for right-angled triangles.
```

> Pythagoras' formula is $$x^2 + y^2 = z^2$$ for right-angled triangles.

The formula must be enclosed in both dollar signs and backticks.
On a standard UK keyboard, the backtick key is in the top left corner
just above the TAB key.

You can also write a formula on a line of its own, which centers it,
by putting three backticks and the word **math** on the line before
(no space between the backticks and the word) and three backticks
on the line afterwards. If you do this, **you must have an empty line both
before and after the formula too**.

    ```math
    e^{i \pi} + 1 = 0
    ```

$$
e^{i \pi} + 1 = 0
$$

Superscripts are indicated with `^` and subscripts with `_`. As soon
as you want more than one character in a super/subscript, surround the
whole thing with `{}` curly braces.

Example: `x^2, y_1, x^{a+1}` gives $$x^2, y_1, x^{a+1}$$.

Special symbols (sums, integrals, Greek letters, functions like sin and log
 etc.) are prefixed with a backslash. There is a long list of features 
[here](https://katex.org/docs/supported.html).

    ```math
    \sum_{i=0}^{\infty} 2^{(-i)} = 2
    ```

$$
\sum_{i=0}^{\infty} 2^{(-i)} = 2
$$

## What is gitlab, anyway?

[Git](https://git-scm.com/) is a version control system invented by Linus
Torvalds, the creator of Linux. Version control is a way to keep track of
files in a project, for example the source code of some software. It lets
you see the complete history of your project, ask who changed which files
when and why, make backups, and have several people working on the same
code independently then merge their results at the end.

To achieve this, Git keeps the complete history of your code in something
called a repository, of which there is typically a copy on every developer's
computer and another copy hosted online somewhere.

At the very latest when you start your second year, you should be familiar
with Git so that you can use it in your software project. Familiarity with
Git is also essential for many jobs involving software development.

[Gitlab](https://gitlab.com) is one of the big three free services where
you can, among other things, host copies of your repositories online.
The other two are [Github](https://github.com) and
[BitBucket](https://bitbucket.org).
For your own code, you can use any service that you like (as long as your
whole team uses the same one, in a group project) but the online discussion
for this unit is hosted on Gitlab so you need a free account there to take
part.
