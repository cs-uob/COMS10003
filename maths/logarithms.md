# Logarithms

*This page is adapted from the documentation for the COMS20002 CNuT unit.*

Back in primary school, I learnt to count with [Cuisenaire rods](https://en.wikipedia.org/wiki/Cuisenaire_rods). They are little rods that come in different lengths from 1 to 10 cm and each length is a different colour. To calculate $$5 + 3$$, you put a 5-rod on the end of a 3-rod, and notice that this is as long as an 8-rod:

![rods](rods.png)

For slightly larger numbers, or if you don't have a box of rods to hand, you can use two rulers. To compute $$3+5$$, line up the rulers so the zero of the second ruler is below the 3 on the first ruler. Then find the 5 on the second ruler; the number above it will be the sum:

![rulers](rulers.png)

Can we do the same for multiplication?

If we tried to build a set of multiplication rods, then we'd notice that a 1-rod would have to have no width at all, as multiplying by 1 does not change a number (for the same reason, there is no 0-rod in the original set).

Let's arbitrarily decide that a 2-rod should be 1cm long. Whether we use cm, inches or some other unit of measure shouldn't matter, after all.

A 4-rod should now be 2cm long, as $$2 \times 2$$ means put two 2-rods in a line, and the result is 2cm long. Similarly, $$8 = 2 \times 2 \times 2$$ will be 3cm long. In fact, $$2^x$$ for integer $$x$$ should be $$x$$ cm long this way:

![multiplication rods](mulrods1.png)

So how long should a 3-rod be? It must be something between 1cm and 2cm (because $$2 < 3 < 4$$). Specifically, it should be $$x$$ cm long such that $$2^x = 3$$. If we try out numbers until we get close, we find that $$x$$ is around 1.584963. Two of thes 3-rods in a line would be around 3.17cm long, which is just a bit longer than an 8-rod at 3cm, which is about the right size for a 9-rod.

## Definition of logarithms

Of course, there is a more precise way to describe these lengths:

  * A rod of length $$y$$ represents the number $$2^y$$.
  * The rod for number $$x$$ is a number $$a$$ cm long such that $$2^a = x$$.
  * This number $$a$$ is called the logarithm (to basis 2) of $$x$$,
    written $$\log_2(x)$$. So the $$x$$-rod is $$\log_2(x)$$ cm long.

Here is a table of $$\log_2(x)$$, pronounced "the logarithm to base 2 of x" for
the first few integers:

|  x | log2(x) |  x | log2(x) |  x | log2(x) |  x | log2(x) |
|---:|:--------|---:|:--------|---:|:--------|---:|:--------|
|  1 | 0.0     | 11 | 3.45943 | 21 | 4.39232 | 31 | 4.95420 |
|  2 | 1.0     | 12 | 3.58496 | 22 | 4.45943 | 32 | 5.0     |
|  3 | 1.58496 | 13 | 3.70044 | 23 | 4.52356 | 33 | 5.04440 |
|  4 | 2.0     | 14 | 3.80735 | 24 | 4.58496 | 34 | 5.08746 |
|  5 | 2.32193 | 15 | 3.90689 | 25 | 4.64386 | 35 | 5.12929 |
|  6 | 2.58496 | 16 | 4.0     | 26 | 4.70044 | 36 | 5.16993 |
|  7 | 2.80735 | 17 | 4.08746 | 27 | 4.75489 | 37 | 5.20945 |
|  8 | 3.0     | 18 | 4.16992 | 28 | 4.80735 | 38 | 5.24793 |
|  9 | 3.16992 | 19 | 4.24793 | 29 | 4.85800 | 39 | 5.28540 |
| 10 | 3.32193 | 20 | 4.32193 | 30 | 4.90690 | 40 | 5.32193 |

The choice of basis for a logarithm is simply choosing which rod we want to make 1cm long. If we had chosen basis 3, then the 3-rod would be 1cm long and all rods would be shorter by a factor of 1.58496 than they are now.

We can do the same thing with rulers. Here is $$3 \times 4 = 12$$:

  * Find the first factor (3) on the first ruler and line up the neutral element (1) of the second ruler below it.
  * Find the second factor (4) on the second ruler, the number above (12) is the product.

![slide rule](sliderule.png)

These rulers are drawn to a scale of 3.8:1 (the distance between 1 and 2 is 3.8cm). But as long as both rulers use the same scale, it really doesn't matter - 3 times 4 is 12 whatever logarithm you use to compute this, after all. In terms of logarithms, these rulers simply use a different basis.

Two rulers with a logarithmic scale that you can slide against each other are called a [slide rule](https://en.wikipedia.org/wiki/Slide_rule). These were a common calculating device before electronic calculators were invented.

## Rules for logarithms

Exponentiation to any basis $$B > 0$$ has the nice properties

  * $$B^0 = 1$$
  * $$B^{(u+v)} = B^u B^v$$

People can make a fuss about what $$0^0$$ is supposed to be. In analysis, keeping
$$0^0$$ undefined makes sense (the function $$x^y$$ cannot be continuous when
both variables are zero) but in algebra and combinatorics we can safely define
$$0^0 = 1$$.

Computer Scientists have lots of reasons to use the basis $$B = 2$$. Other people
sometimes use basis $$B = 10$$, but mathematicians use a special basis that they
call $$e$$ which is Euler's number, around 2.7183. This has some nice properties
once integrals get involved.

The logarithm functions (for any fixed basis $$B > 0$$) are functions with the properties

  * $$\log(ab) = \log(a) + \log(b)$$
  * $$\log(1) = 0$$

With $$\log_B(B^x) = x$$, $$B^{\log_B(y)} = y$$ we see that exponentiation and
logarithm are inverses of each other.

## Change of basis

The change-of-basis formula is $$\log_B(x) = \log_A(x)/\log_A(B)$$. For example, if you have a calculator that does logarithms to basis $$A = 10$$, then to calculate $$\log_2(x)$$ you divide $$log10(x)$$ by $$log10(2)$$. The constant $$log10(2)$$ is 0.6931472, or equivalently you can multiply with the inverse: to compute $$\log_2(x)$$, compute $$log10(x)$$ on the calculator and multiply with 1.442695.

To compute logarithms of fractions, $$\log_2(a/b) = \log_2(a) - \log_2(b)$$. So for example to compute $$\log_2(5/11)$$, we could look up in the table above that $$\log_2(5) = 2.32193$$ and $$\log_2(11) = 3.45943$$ so $$\log_2(5/11) = 2.32193 - 3.45943 = (-1.1375)$$. The logarithm of something less than 1 will always be negative.
