# Boolean logic

Boolean logic is Boolean algebra, viewed as logic. The operations still do the same things, but the questions we are asking are different.

*For the 2019-20 academic year, this page contains extra material for interested students and you do not need to learn it yet (although if any material on this page also appears in a lecture, then you do need to know it). This material might help to prepare you for the semantics part of the second-year unit Language Engineering.*

<style>
.markdown-section table {
    width: auto;
}
.markdown-section th {
    border-bottom: 1px solid #000 !important;
}
</style>

## Terms

Over the integers, $$1+2$$ is a valid term but $$+2+$$ is not. In Boolean algebra, $$0 \wedge 1$$ is a valid term but $$\wedge \vee$$ is not.
If this sounds obvious to you, it is because you are a human; computers do not have "common sense" so if you want a computer to do
calcluations for you then you need to specify exactly what is or is not a term. Here is one way to do that.

A term in Boolean logic is anything you can build with the following rules:

  1. 0 and 1 are terms.
  2. If $$T$$ is a term then $$(\neg T)$$ is also a term.
  3. If $$T, U$$ are terms then $$(T \wedge U)$$ and $$(T \vee U)$$ are also terms.

This only defines AND, OR and NOT but we could add extra rules for the other operations, or we could make one "super-rule" that
says if $$T, U$$ are terms and $$\diamond$$ represents any binary operation then $$(T \diamond U)$$ is also a term.

For example, $$(1 \wedge (0 \vee 1))$$ is a term because we can construct it like this:

  * 0 and 1 are terms by rule 1.
  * Therefore, by rule 3 $$(0 \vee 1)$$ is a term.
  * Therefore, by rule 3 $$(1 \wedge (0 \vee 1))$$ is also a term.

To be precise, in the last step we applied "if $$T, U$$ are terms then $$(T \wedge U)$$ is a term with $$T = 1$$
and $$U = (0 \vee 1)$$, and we've already established that both of these are terms.

With the rules above, we only allow fully bracketed terms: $$(1 \vee (1 \vee 1))$$ is a term but
$$1 \vee 1 \vee 1$$ is not. We could solve this problem by adding further rules for dropping brackets,
or adding rules that you can make terms using binary operators without adding extra brackets, but that
would complicate the point that we are working towards.

So far, we deliberately haven't allowed variables in terms. If we want them, we can just add a new rule:

  * A variable is a term.

## Representing terms

Suppose we want to write a computer program to work with terms in Boolean logic. Terms are defined
by the three rules (4 if you want variables) given above.
Different programming languages can represent terms in different ways. If you are familiar with
object-oriented programming in Java you could do terms like this (you will learn Java in teaching block 2):

```java
// Interface for terms so that we can build larger terms out of smaller ones.
interface Term {}

// Rule 1: values true/false are terms.
class ValueTerm implements Term { 
  final boolean value; 
  ValueTerm(boolean value) { this.value = value; } 
}
// Rule 2: negation of a term is a term.
class NotTerm implements Term {
  final Term child;
  NotTerm(Term child) { this.child = child; }
}
//Rule 3: AND and OR of terms are terms.
// We'll make an abstract class for binary operations
// so that we can add more if we want.
abstract class BinaryTerm implements Term {
  final term left;
  final term right;
  BinaryTerm(Term left, Term right) {
    this.left = left; this.right = right;
  }
}
class AndTerm extends BinaryTerm {
  AndTerm(Term left, Term right) { super(left, right) }
}
class OrTerm extends BinaryTerm {
  OrTerm(Term left, Term right) { super(left, right) }
}
```

To create the term $$(1 \vee (\neg 0))$$, we could write the following Java code:

```java
Term t = new OrTerm(
  new ValueTerm(true),
  new NotTerm(new ValueTerm(false))
)
```

You can also do terms very easily in functional programming languages. Here's the Haskell version:

```haskell
-- Define the data type.
data Term = ValueTerm Bool | NotTerm Term | AndTerm Term Term | OrTerm Term Term
-- Here's the term (1 OR (NOT 0))
OrTerm (ValueTerm true) (NotTerm (ValueTerm false))
```

## Evaluating terms

The three rules which describe the structure of terms also give us a way to evaluate a term. We just have to say what to do with each type of term.
We can write evaluation as a function $$e$$ from terms to Boolean values (0/1) if we want.

  * The term 0 evaluates to the value 0, and the term 1 evaluates to the value 1.
    If we write values in bold just to be pedantic for once, then $$e(0) = \mathbf 0$$ and $$e(1) = \mathbf 1$$.
  * To evaluate a term of the form $$(\neg T)$$, first evaluate $$T$$ and then take the opposite value, e.g. $$e(\neg 0) = \mathbf 1$$. The general formula is $$e(\neg T) = \neg e(T)$$, e.g. $$e(\neg 0) = \neg e(0) = \neg \mathbf 0 = \mathbf 1$$.
  * To evaluate a term of the form $$(T \wedge U)$$, first evaluate $$T$$ and $$U$$ and then apply the $$\wedge$$ operation to the result: $$e(T \wedge U) = e(T) \wedge e(U)$$. The same principle applies for $$(T \vee U)$$.

This all sounds like a really complicated way of describing something quite simple, but the point is it's detailed enough that you could program a computer to do it. 

Actually, there is a subtle point here. Consider the evaluation rule for NOT terms $$e(\neg T) = \neg e(T)$$. The two negation symbols are actually not the same thing.
On the left, we have the term $$(\neg T)$$ which we can think of as a string of symbols: an opening bracket, the negation symbol, the symbols that make up the term T, and a closing bracket.
These symbols on their own do not "do" anything: they are data, not code. Instead, the evaluation function does something to them.
On the right, the negation symbol stands for the Boolean negation function that turns 0 into 1 and vice versa. This evaluation rule reads: to evaluate a term (string of symbols) that
starts with a negation and then contains some other term T, evaluate the inner term T first and then apply the Boolean negation function to the result of that.
The same discussion of course also applies to AND and OR terms.

## Evaluating terms with variables

(1 + 2) is a term in integer arithmetic that we can evaluate to 3. (1 + x) is a term with one variable x, and to
evaluate it we would need to know the value of x. (x + (-x)) is also a term with a variable x, but it will always evaluate to 0 no matter what x we choose.

$$x \wedge 1$$ is a term in Boolean logic with one variable. To be precise, it is a term with one **free variable**. We cannot evaluate terms with free variables directly.

An **environment** or **interpretation** of a term is an assignment of values to all its free variables. For example, $$x = 1$$ is an environment in which we can evaluate the term $$x \wedge 1$$, giving the value 1. The rule for evaluation is: substitute all the free variables with the values given by the environment, then evaluate the term as before.

## Syntax and semantics

There are many different kinds of logics: Boolean, propositional, predicate, modal etc.
All have in common that they contain two important concepts that are not to be confused:

  * The **syntax** of a logic defines the rules for forming a term in this logic, and possibly rules for transforming terms into other terms.
  * The **semantics** of a logic defines a way to interpret or evaluate terms.

In Boolean logic, the syntax is the three (or four, with variables) rules for forming terms. The semantics is the evaluation function
that turns terms in Boolean logic into truth values.

## Semantic equality

To make things a bit clearer, we concentrate on terms without variables for a moment.

Two terms are **syntactically equal** if they are the same string of symbols. We can use the normal equals sign for this.
Two terms are **semantically equal** if they evaluate to the same truth value. We use the equals sign with three bars $$\equiv$$ for this.

Saying that two terms are **equivalent** means the same thing as semantically equal, and this is the standard way of pronouncing
the $$\equiv$$ sign.

Syntactic equality implies semantic equality but not the other way round. For example, $$1 = 1$$ and so also $$1 \equiv 1$$
but $$1 \equiv (\neg 0)$$ even though the two terms are not syntactically equal.

Without variables, every term is in one of two classes: true terms and false terms. Every term is either equivalent to 0 or to
1, but never both.

With variables, things become a bit more complicated. Two terms are equivalent if their truth tables are the same
for all variables involved in either of the terms, for example $$(x \vee y) \equiv (y \vee x)$$. This means that for every
possible assignment/interpretation of the variables, the two terms are equivalent.
However, two terms can be equivalent even if they don't have the same set of variables, for example
$$x \equiv x \wedge (y \vee \neg y)$$. The rule for checking equivalence of two terms is that you make one truth table with all the
variables that appear in one or both of the terms, with one column for each term. If these two columns are equal, then the terms are
equivalent.

## Semantic implication

One term A **semantically implies** another term B if, for every possible variable assignment that makes A true, B is also true.
We write this with the symbol $$A \vDash B$$, pronounced "A implies B".

For example, $$x \wedge y \vDash x$$ because the truth table is

| $$x$$ | $$y$$ | $$x \wedge y$$ | $$x$$ |
|-------|-------|----------------|-------|
|0|0|0|0|
|0|1|0|0|
|1|0|0|1|
|1|1|1|1|

The points to remember here are, for any terms A, B:

  * If $$A \equiv B$$ then also $$A \vDash B$$, but not necessarily the other way round.
  * In fact, $$A \equiv B$$ if and only if ($$A \vDash B$$ and $$B \vDash A$$).
  * $$A \vDash B$$ holds if and only if the term $$(A \rightarrow B)$$ is a tautology.

$$\vDash$$, like $$\equiv$$, combines two terms into an "equation", not a term. Just like
1+x is a term but 1+x = 2 is an equation and 1+x > 2 is an inequality, $$(x \wedge 1)$$ is a term,
$$x \equiv y$$ is an "equation" of terms and $$x \vDash 1$$ is an "inequality" of terms.

Semantic implication gives you many new rules such as $$A \wedge B \vDash A$$. This says that
if you know A is true and B is also true, then you can conclude that A is true. On its own,
this doesn't tell you anything useful, but in a larger proof it can be useful to use rules like
these. Of course, $$A \wedge B \vDash B$$ is also a rule.

## Models

The symbol $$\vDash$$ is often used for up to three different things in books on logic.
The first is the use we mentioned above, to show that one term implies another.
The second use is to write $$\vDash A$$ to mean that A is a tautology. This is quite harmless:
you can read it as "A follows from nothing", which makes sense as a tautology is always true.
The third use is that if A is a term and M is an assignment of values to all the free variables in A, then one writes $$M \vDash A$$ to mean that A is true if you assign the variables with the values in M. In this case the symbol is read "M is a **model** for A". Under this use, for two terms A, B, the statement $$A \vDash B$$ means that every model for A is also a model for B.

## Semantic rules

With a truth table we can quickly find rules such as $$x \wedge y \equiv y \wedge x$$ where
$$x, y$$ are variables. Any rule like this that applies to free variables automatically
also applies to whole terms: if $$T, U$$ are any two terms then $$T \wedge U \equiv U \wedge T$$.

The rules at the end of the page on [Boolean algebra](Boolean.md) thus are actually rules for
terms, not just for variables. And we can use them within larger terms: given 
$$(0 \vee ((a \wedge b) \vee (\neg (a \wedge b))))$$ we can apply the either-or rule $$(T \vee (\neg T)) \equiv 1$$
to the $$\vee$$ operation in that term, with $$T$$ standing for the subterm $$(a \wedge b)$$, to get

$$
(0 \vee ((a \wedge b) \vee (\neg (a \wedge b)))) \equiv (0 \vee 1)
$$

which we can now evaluate to 1, as all the free variables are gone.

## Syntactic rules

Although the rules such as either-or, associative etc. are derived from semantic principles, and can
be proven with truth tables, in logic we are also allowed to have a list of purely syntactic rules
to work with terms. Basic rules that come with the logic are called **axioms** and rules that are
derived from other rules are usually called **theorems**.

For example, one could teach Boolean logic by introducing the values 0/1, the operations AND, OR
and NOT and then giving a list of axioms, without mentioning truth tables and semantics.
Boolean logic would then be the theory of terms as strings of characters which you are allowed
to transform into other strings using the rules provided.

This might seem like an odd way to teach logic, and for Boolean logic it would indeed be,
but when you learn arithmetic you also learn rules like a + b = b + a without proving them
with truth tables - which in any case would not work because there are infinitely many numbers
so you cannot list them all in a table.

A syntactic rule is a rule saying that if you have a term in a particular format, then you can
replace it with a term in another format without having to think what the term means. Most of
the rules that we know are actually pairs of such replacement rules. For example, the associative rule
$$x \wedge (y \wedge z) \equiv (x \wedge y) \wedge z$$ says that:

  1. If you have a term of the form $$x \wedge (y \wedge z)$$, that is an AND term whose right-hand side
     is again an AND term, then you can replace it with $$(x \wedge y) \wedge z$$.
  2. If you have a term of the form $$(x \wedge y) \wedge z$$, that is an AND term whose *left*-hand side
     is again an AND term, then you can replace it with $$x \wedge (y \wedge z)$$.

A syntactic rule that says if you have a term $$T$$ then you can turn it into a term $$U$$ can be
written $$T \vdash U$$. How to pronounce this symbol is a matter of debate between different books.
Some use a name like "turnstile", but this is awkward. I suggest "becomes".

For example, as purely syntactic rules, the associative rules for AND are
$$x \wedge (y \wedge z) \vdash (x \wedge y) \wedge z$$ and
$$(x \wedge y) \wedge z \vdash x \wedge (y \wedge z)$$.
These two rules just happen to be a pair of "opposite" rules, but a logic is allowed to have 
syntactic rules that do not come in pairs. For example, one could have a rule $$x \wedge y \vdash x$$
which says that if you can "cancel" out of an AND term. The resulting term will no longer be equivalent
but in a proof, if the term at the beginning was true and you apply rules like this, then the term
at the end will still be true.

This is a bit like taking the square on both sides of an equation. If $$x = 2$$ and you square to
get $$x^2 = 4$$ then as long as the original equation held, the new one will also hold, but from
$$x^2 = 4$$ you cannot go back to $$x = 2$$ as $$(-2)$$ also satisfies the squared equation.

## Proofs

A proof in a formal logic is a sequence of steps, each of which is justified by a rule.
The proof itself then becomes further rule, that can be used in other proofs.
For example given the axioms

  * assoc.l.and: $$(x \wedge y) \wedge z \vdash x \wedge (y \wedge z)$$
  * assoc.r.and: $$x \wedge (y \wedge z) \vdash (x \wedge y) \wedge z$$
  * comm.and: $$x \wedge y \vdash y \wedge x$$

We can prove that $$(x \wedge y) \wedge z \vdash (z \wedge y) \wedge x$$ as follows:

|rule|term|
|----|----|
||$$(x \wedge y) \wedge z$$|
|assoc.l.and|$$x \wedge (y \wedge z)$$|
|comm.and|$$(y \wedge z) \wedge x$$|
|comm.and|$$(z \wedge y) \wedge x$$|

## Completeness and soundness

A logic, such as Boolean logic, can be seen as a data structure with

  * A set of rules for forming terms.
  * A set of rules for transforming terms, called axioms.
  * A set of rules for interpreting terms as true or false.

The first two points form the syntax, the third is the semantics. For some logics, there are other ways to interpret a term besides true or false, but that does not concern us here.

Of course, it helps if the syntax and semantics are compatible:

  * A logic is called **complete** if every true term can be proven from the axioms.
  * A logic is called **sound** if every term that can be proven from the axioms is also true.

You might ask why a logic shouldn't obviously be both complete and sound, but Kurt Gödel's famous incompleteness theorem says, in a very simplified form: every logic that is powerful enough to be able to reason about the integers, and is sound, cannot also be complete. In other words, if mathematicians agree they will never do anything that lets them prove a false statement, then there will always be true statements which they will never be able to prove (even in theory).

# Propositional logic

Propositional logic is the first in a line of logics that make up the foundations of mathematics. (The next in the line is predicate logic.)

Propositional logic is essentially Boolean logic, with the variables standing for propositions such as "It is Monday" that can be true or false. The implication operation $$\rightarrow$$ is used a lot for convenience, but this does not change the logic as it is just an abbreviation for
$$x \rightarrow y \equiv (\neg x) \vee y$$.

However, the syntax rules of propositional logic are often expressed in a way where you can derive a conclusion from a set of terms rather than just single term. In some contexts you can think of a set of terms as the logical AND of all the terms in the set, but that way of thinking is not helpful for actually writing proofs.

Some important rules in propositional logic include:

**Modus ponens**, or **Syllogism**: $$\{p, p \rightarrow q\} \vdash q$$. This says that if you know p is true and you know that p implies q, then you can conclude that q is true. This is perhaps the most fundamental rule for logical reasoning. You could write this as $$p \wedge (p \rightarrow q) \vdash q$$ but this is less helpful to do proofs.

The classic example: Socrates is a man, all men are mortal (man $$\rightarrow$$ mortal), therefore Socrates is mortal. (The ancient Greeks verified this particular statement with an experiment.)

**Modus tollens**: $$\{p \rightarrow q, \neg q\} \vdash \neg p$$. This says that if you know p implies q, but q is false, then you can conclude that p is also false.

**Contraposition**: $$p \rightarrow q \vdash \neg q \rightarrow \neg p$$. This says that if you know p implies q, then you know the negation of p implies the negation of p. If you know the value of p or q then this "collapses" into modus ponens or modus tollens, but the real use of this rule is that if you want to prove that a implies b, it lets you prove that not-b implies not-a instead if that is easier to show. 

Formally, you set p and q to be not-b and not-a respectively, or you realise that this rule actually works both ways round. Looking at semantics for a moment, what is really going on here is $$a \rightarrow b \equiv \neg b \rightarrow \neg a$$ which means that every statement is equivalent to its contrapositive.

**Contradiction**: $$\neg p \rightarrow 0 \vdash p$$. If not-p would lead to a false statement, then p must be true. This lets us prove p by starting with "Assume that p is false", calculate until we get something obviously false (a contradiction) and then conclude "therefore, our assumption must be false, so p is true".
