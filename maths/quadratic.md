# Quadratic equations

A quadratic equation is an equation of the form $$a X^2 + b X + c = 0$$
where $$a$$ is non-zero.

When plotted, a quadratic equation gives a parabola or "U shape", which may
point upwards or downwards.

## Theory

To solve a quadratic equation in theory, we first we divide by $$a$$ (it's
non-zero so this is fine) to get a new equation $$X^2 + uX + v = 0$$ with
$$u = b/a$$ and $$v = c/a$$.

Next, we try and factor the equation.
The binomial formula says that $$(X + y)^2 = X^2 + 2Xy + y^2$$.
Sometimes, we get lucky: consider $$X^2 + 6X + 36 = 0$$. If we
set $$y = 3$$, then this is exactly $$(X + 3)^2$$, so our equation
factors as $$(X + 3)(X + 3) = 0$$.

If we have an equation $$(X + p)(X + q) = 0$$, then we can use the following
fact: a product of two numbers is 0 if and only if at least one of the factors
is 0. For the equation $$(X + p)(X + q) = 0$$ to hold, either $$(X+p)$$ has
to be $$0$$ in which case $$X = (-p)$$ or $$(X+q)$$ has to be $$0$$ in which case $$X = (-q)$$. The solutions in this case are $$(-p)$$ and $$(-q)$$.

This lets us already solve some cases:

  * $$(X+2)(X+2) = 0$$ has exactly one solution, $$X = (-2)$$.
  * $$(X+2)(X-3) = 0$$ has two solutions, $$(-2)$$ and $$3$$.
    A bracket with a minus sign gives a positive solution as $$X - 3 = 0$$
    has the solution $$X = 3$$.
  * $$X^2 + 4X + 4 = 0$$ factors as $$(X + 2)(X + 2) = 0$$ using the
    binomial formula, which gives $$(-2)$$ as the only solution.

To find the solutions in the general case, we use a method called
*completing the square*. Given $$X^2 + uX + v = 0$$ we try and apply the
binomial formula by setting $$y = u/2$$ to get $$X^2 + 2yX + v = 0$$.
Then, we add $$y^2 - v$$ on both sides to get the equation
$$X^2 + 2yX + y^2 = y^2 - v$$. This lets us apply the binomial formula
on the left to get $$(X+y)^2 = y^2 - v$$. 

The equation $$(X + y)^2 = z$$ has the following solutions:

  * If $$z = 0$$, a single solution $$(-y)$$.
    This is the case we discussed earlier.
  * If $$z < 0$$, no solutions over the real numbers since the square
    of something cannot be negative over the real numbers. (We will talk
    about complex numbers later in the unit, where a square can be negative.)
  * If $$z > 0$$, we can take the square root, but bear in mind that if
    $$r^2 = z$$ then $$(-r)^2 = z$$ too, so there is a positive and a 
    negative root. The solutions are $$z + \sqrt{y}$$ and $$z - \sqrt{y}$$.

For example, consider $$X^2 - X - 6 = 0$$ with $$u = (-1), v = (-6)$$.
Setting $$y = u/2 = (-1/2)$$ and adding $$y^2 - v = 1/4 + 6 = 25/4$$
on both sides gives $$X^2 - X + 1/4 = 25/4$$ which is $$(X - 1/2)^2 = 25/4$$.
The right side is positive so we get $$X - 1/2 = \sqrt{25/4} = 5/2$$ with
solution $$6/2 = 3$$ and $$X - 1/2 = -\sqrt{25/4} = -5/2$$ with solution $$(-2)$$.
The solutions to this equation are $$3, (-2)$$.

## Formula

The formula for quadratic equations, if you substitute the above back into
the original equation, works like this.

Given $$a X^2 + bX + c = 0$$, first compute the **discriminant**
$$d = b^2 - 4ac$$.

  * If $$d < 0$$, there are no solutions in the real numbers.
  * If $$d = 0$$, there is one solution $$(-b)/(2a)$$.
  * If $$d > 0$$, the two solutions are $$ \frac{(-b) \pm \sqrt{d}}{2a}$$

In fact, in all three cases the formula can be summarised as

$$
\frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
$$

which gives only one solution if the root becomes zero, and no real solutions
if the number under the root is negative.

Taking $$X^2 - X - 6 = 0$$ again, we have $$d = (-1)^2 - 4(-6) = 25$$ which
is positive, and $$(1 \pm \sqrt{25})/2$$ gives $$3$$ and $$(-2)$$ as solutions.
