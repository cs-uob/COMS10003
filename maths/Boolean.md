# Boolean algebra

<style>
/* horrible hack because stupid */
.exercise + p {
    margin-bottom: 0;
    background-color: #F2F2F2;
    border-left: 5px solid #79B6DF;
    padding-left: 1em;
}

.exercise + p + ul {
    background-color: #F2F2F2;
    border-left: 5px solid #79B6DF;
}

.markdown-section table {
    width: auto;
}
.markdown-section th {
    border-bottom: 1px solid #000 !important;
}
</style>

_This page contains additional notes on Boolean algebra and logic that can be read together with the lecture slides/videos of the first part of this unit._

We already know about integers:

  * 1, 2, 3 etc. are integers
  * addition is a function that takes two integers in and outputs one new integer, e.g. 2 + 3 = 5. We call addition a _binary function_, which means it takes two inputs.
  * Negation (e.g. the negation of 5 is (-5)), squaring, doubling, increasing by one etc. are _unary functions_: they take one input and produce one output.

Boolean algebra is a new kind or mathematics: there are only two "numbers". Different people call them by different names however, depending on which book you read they will be called

  * 1 and 0
  * **true** and **false**
  * T and F
  * $$\top$$ and $$\bot$$

In place of addition, doubling etc. we have new operations too:

  * $$\wedge$$, pronounced **and** is a binary operation with $$1 \wedge 1 = 1$$ and all other combinations result in $$0$$, e.g. $$0 \wedge 0 = 0, 0 \wedge 1 = 0, 1 \wedge 0 = 0$$.
  * $$\vee$$, pronounced **or**, is a binary operation with $$0 \vee 0 = 0$$ and the other three possibilities all give $$1$$.
  * $$\neg$$, pronounced **not**, is a unary operation with $$\neg 0 = 1$$ and $$\neg 1 = 0$$.

We can list all possibilities in a table, which is called a **truth table** for the operation.
The "not" operation takes only one input, so there are two possibilities:

| $$x$$ | $$\neg x$$ |
|-------|------------|
| 0     | 1          |
| 1     | 0          |

The binary (two inputs) operations have four possibilities for the two inputs:

| $$x$$ | $$y$$ | $$x \wedge y$$ | $$x \vee y$$ |
|-------|-------|----------------|--------------|
| 0     | 0     | 0              | 0            |
| 0     | 1     | 0              | 1            |
| 1     | 0     | 0              | 1            |
| 1     | 1     | 1              | 1            |

Just like you calculate (1 + 2) + 4 = 3 + 4 = 7, you can calculate more complicated expressions in Boolean algebra, for example $$0 \vee (1 \wedge \neg 0) = 0 \vee (1 \wedge 1) = 0 \vee 1 = 1$$.

<div class="exercise"></div>

**Exercise:**

  * Compute $$0 \vee (1 \wedge 0)$$, $$(1 \wedge 1) \vee (1 \vee 1)$$ and $$\neg (1 \vee \neg 0)$$.
  * Find values for $$x, y$$ such that $$\neg x \vee y$$ becomes 0.
  * If a term in Boolean algebra has $$n$$ distinct variables, how many rows does do we need in a truth table (not counting the header row) to show all possibilities? For example, how many do we need for $$x \vee (y \wedge \neg z) \vee (\neg y \wedge z)$$?

# Satisfiable formulas and tautologies

If a formula in Boolean logic has a 1 output for all its inputs, then we call it a **tautology**.
For example, $$x \vee \neg x$$ has the truth table

| $$x$$ | $$x \vee \neg x$$ |
|-------|------------|
| 0     | 1          |
| 1     | 1          |

This means that whatever the input(s), the output will always be 1 (resp. true).
On the other hand, a formula is called **unsatisfiable** if its output is always 0 whatever the inputs, e.g. $$x \wedge \neg x$$. A formula that is not unsatisfiable is called **satisfiable**,
and a formula that is satisfiable but not a tautology (both 0 and 1 appear in the outputs) is sometimes called a **contingency**. In other words:

| outputs      | satisfiable | unsatisfiable | tautology | contingency |
|--------------|-------------|---------------|-----------|-------------|
| all 0        | no          | yes           | no        | no          |
| both 0 and 1 | yes         | no            | no        | yes         |
| all 1        | yes         | no            | yes       | no          |

Think of these terms like this: there are three types of formulas - unsatisfiable (all 0), contingency (both), tautology (all 1). The term satisfiable applies to both the types that are not unsatisfiable.

If a formula $$F$$ is a tautology, then $$\neg F$$ is unsatisfiable and vice versa. If you negate a contingency, it remains a contingency.

# Equivalence

Two formulas are equivalent if they have 0s and 1s in the same places in their truth tables. To say that $$F, G$$ are equivalent we write $$F \equiv G$$.
For example,

  * $$x \vee 1 \equiv 1$$
  * $$x \vee 0 \equiv x$$
  * $$\neg (\neg x) \equiv x$$
  * $$x \wedge y \equiv \neg (\neg x \vee \neg y)$$

(The words formula, term and expression mean the same thing in this document.)

# Some more operations

How many possible unary (1 input) resp. binary (2 inputs) Boolean operations are there?

A unary operation $$f$$ must have a truth table like this:

| $$x$$ | $$f(x)$$ |
|-------|----------|
| 0     | $$a = f(0)$$ |
| 1     | $$b = f(1)$$ |

Here we have two parameters $$a, b$$ to decide on, giving four possible functions:

|f(0)|f(1)|name|
|--------|--------|----|
|0|0|constant 0|
|0|1|identity  |
|1|0|negation  |
|1|1|constant 1|

For binary functions, we have four rows in the truth table so four parameters for the possible outputs giving $$2^4 = 16$$ possible functions, some of which we give names to:

|f(0,0)|f(0,1)|f(1,0)|f(1,1)|name|
|------|------|------|------|----|
|0|0|0|0|constant 0|
|0|0|0|1|AND|
|0|0|1|0||
|0|0|1|1|first argument|
|0|1|0|0||
|0|1|0|1|second argument|
|0|1|1|0|XOR (exclusive OR)|
|0|1|1|1|OR|
|1|0|0|0|NOR|
|1|0|0|1|equal arguments|
|1|0|1|0|NOT second argument|
|1|0|1|1|reverse implication|
|1|1|0|0|NOT first argument|
|1|1|0|1|implication|
|1|1|1|0|NAND|
|1|1|1|1|constant 1|

Let's look at a few of these in more detail as they're useful in hardware design (and sometimes software too).

| $$x$$ | $$y$$ | $$x\ \textrm{XOR}\ y$$ | $$x\ \textrm{NAND}\ y$$ | $$x\ \textrm{NOR}\ y$$ |
|-------|-------|----------------|--------------|-----|
| 0     | 0     | 0              | 1            | 1   |
| 0     | 1     | 1              | 1            | 0   |
| 1     | 0     | 1              | 1            | 0   |
| 1     | 1     | 0              | 0            | 0   |

The exclusive-OR operation XOR, sometimes also written $$\oplus$$, outputs 1 if either $$x$$ or $$y$$ is 1 but not both. Or put another way, $$x\ \textrm{XOR}\ y$$ is 1 if $$x, y$$ are different and 0 when they are the same.

(In Boolean algebra and in logic, whenever we use the word **or** without further qualification, we always mean an inclusive or, e.g. "x or y or both").

The NAND and NOR operations are the negations of the AND and OR ones. They are worth knowing because of a special property that they have, which we will see in the next section.

A recurring theme in this document will be that different books use different notation for the same thing. Indeed, some books use $$\uparrow$$ and $$\downarrow$$ for NAND and NOR; although I have seen a paper before now that used $$\uparrow$$ to refer to XOR instead.

# Normal forms and functional completeness

If we have only the constants 0 and 1, variables, brackets and a selection of operations, can we build all formulas out of these? It depends on the operations - if the only operation we have is AND then we won't get far for example. No expression containing just the variables x, y, brackets, constants 0, 1 and the AND operation can ever be a formula for (x OR y).

If we have the AND, OR and NOT operations then we can produce every possible formula. Let's look at the case of two variables, but the idea is the same for every number of variables. For example, if we want to produce this one of the 16 possible ones, which is of course XOR:

| x | y | f(x, y) | clause |
|---|---|---------|--------|
| 0 | 0 | 0       | $$\neg x \wedge \neg y$$ |
| 0 | 1 | 1       | $$\neg x \wedge y$$ |
| 1 | 0 | 1       | $$x \wedge \neg y$$ |
| 1 | 1 | 0       | $$x \wedge y$$ |

We can make a clause that matches only a particular row by taking the AND of all the variables invloved and inserting a negation before the ones that should be 0. Then we can make a formula that matches only the rows we want, by taking the OR of the clauses for these rows. For the above formula this gives us

$$
x \ \textrm{XOR}\ y \equiv (\neg x \wedge y) \vee (x \wedge \neg y)
$$

A formula like this, which is a list of clauses joined by OR in which each clause is a list of variables (possibly negated) joined by AND is called a formula in **disjunctive normal form** or DNF. "Disjunction" is another word for "or".

Every formula has an equivalent formula in DNF. Similar to DNF, there is also **conjunctive normal form** or CNF where the clauses are joined by AND and the variables within clauses are joined by OR. Every formula also has an equivalent CNF formula.

If we only have AND and NOT, but no OR, we can still build an OR out of that:

$$
x \vee y \equiv \neg (\neg x \wedge \neg y)
$$

So with just the operations $$\{\wedge, \neg\}$$ we can build all possible operations.
A set of operations like this that is enough to build all possible operations (together with brackets and constants) is called **functionally complete**.

Can we build a functionally complete set with only one operation? Yes, there are in fact two such sets: $$\{\textrm{NAND}\}$$ and $$\{\textrm{NOR}\}$$. This has implications for processor design: in a typical processor, _everything_ is built out of NAND gates which makes manufacturing easier as you only have to be able to build one type of gate. (In theory you could use NOR instead, in practice using NAND has a tiny advantage so everyone does that.)

For example, $$x \ \textrm{NAND}\ x \equiv \neg x$$ gives you the NOT gate; putting a NOT behind a NAND gives you an AND - as a formula,

$$
x \wedge y \equiv (x\ \textrm{NAND}\ y)\ \textrm{NAND}\ (x\ \textrm{NAND}\ y)
$$

but what this formula hides is that in practice you would build an (x AND y) circuit like this:

```
     --------       --------
x -->|      |   |-->|      |
     | NAND |-->+   | NAND |--> output
y -->|      |   |-->|      |
     --------       --------
```

where the gate on the right, a NAND with both inputs connected, is working as a NOT gate.

Since we can build AND and NOT out of NAND; we can build OR out of AND and NOT; and we can build absolutely everything out of AND, OR and NOT through DNF (or CNF), we conclude that the NAND gate on its own is functionally complete.

<div class="exercise"></div>

**Exercise:**

  * Write down a formula equivalent to (x OR y) using only x, y, NAND and brackets.
  * Write down a tautology using only x, NAND and brackets. (This shows that we can build all possible formulas out of only NAND operations without even needing the constant 1, or 0 for that matter as we can negate the tautology using just NAND too.)
  * Construct a formula equivalent to (x AND y) using only NOR gates. Do the same for (NOT x).

# Equivalence and implication

Three more of the 16 possible binary operations are worth looking at more closely.

| $$x$$ | $$y$$ | $$x \rightarrow y$$ | $$x \leftarrow y$$ | $$x \leftrightarrow y $$ |
|-------|-------|----------------|--------------|--------|
| 0     | 0     | 1              | 1            | 1      |
| 0     | 1     | 1              | 0            | 0      |
| 1     | 0     | 0              | 1            | 0      |
| 1     | 1     | 1              | 1            | 1      |

The operator $$x \leftrightarrow y$$ returns 1 if the two sides are the same and 0 otherwise. For example, $$ 1 \leftrightarrow 1 \equiv 1$$ but $$ 0 \leftrightarrow 1 \equiv 0$$. This operation is called **equivalence** or **if and only if**, which mathematicians often shorten to **iff** with two fs.

This operation is the negation of XOR: $$x \leftrightarrow y \equiv \neg (x\ \textrm{XOR}\ y)$$.

We need to make three warnings at this point.

**Warning 1.** 'if' and 'iff' mean two different things in mathematics, as we are about to see.

**Warning 2.** Some textbooks use $$\Leftrightarrow$$ for what we write as $$\leftrightarrow$$.
Other textbooks use $$\Leftrightarrow$$ for what we call $$\equiv$$.

**Warning 3.** $$\equiv$$ and $$\leftrightarrow$$ are not the same thing.

Over the integers, $$1 + x$$ is a term; $$x - 1 = 2$$ is an equation, with a term on each side.
We can transform terms according to certain rules like changing $$1 + x$$ into $$x + 1$$; for equations we can additionally perform the same operation on both sides such as changing $$x - 1 = 2$$ into $$x = 3$$ by adding 1 on both sides. You cannot "add 1 on both sides of a term" - a term does not have sides.

Similarly in Boolean logic, $$x$$ is a term, $$x \wedge y$$ is a term and $$x \leftrightarrow y$$ is also a term. $$\leftrightarrow$$ is a binary operator just like $$\wedge$$ (or $$+$$, over the integers). Connecting terms with a binary operator gives you another term.

The $$\equiv$$ equivalence sign is not an operator, in the sense that it does not produce a term. Just like $$x - 1 = 2$$ is an equation and not a term, $$x \leftrightarrow y \equiv \neg (x\ \textrm{XOR}\ y)$$ is an "equation in Boolean algebra" that says that two terms are equal.
You can do things to both sides of an equation in Boolean algebra 

The two concepts are related in that for two terms $$F, G$$ (or formulas, which means the same thing for us) we have $$F \equiv G$$ exactly when $$F \leftrightarrow G$$ evaluates to 1.

The formula $$x \rightarrow y$$ is pronounced **x implies y** or **if x, then y** - this is the use of 'if' with only one f in mathematics. Implication in mathematics means something different than in everyday language. If I say "if it's raining, I will take my umbrella" I usually mean "if it is raining, I will take my umbrella; if it is not raining, I will not take my umbrella". A mathematician would say 'iff' not 'if' for this meaning!

In mathematics, "if it's raining then I'll take my umbrella" says nothing about what you might or might not do if it's not raining. In fact that statement is also true if you take an umbrella every day, whether it's raining or not. Formally $$x \rightarrow y \equiv (\neg x) \vee y$$, so "if x then y" means "x is false, or y is true, or both". The only way the statement "if x then y" can be false is if x is true and y is false.

$$x \leftarrow y$$ is reverse implication and means the same thing as $$y \rightarrow x$$. Lots of books don't bother with this symbol because the left-facing arrow is also useful for other things in other parts of mathematics. And of course there are people who write $$x \Rightarrow y$$ instead of $$x \rightarrow y$$.

# Rules for terms

In arithmetic, we can change 2+x into x+2, x + (-x) into 0 and so on. What we are doing here is changing a term into another, equivalent term using a set of rules such as $$a+b \equiv b+a$$.

In Boolean logic, there is a list of standard rules too, but we can be more precise about what "rule" means: we say two terms $$F, G$$ are equivalent ($$F \equiv G$$) when both terms have 1s and 0s in the same rows of their truth tables. This is the same thing as saying that for every possible assignment to all the variables in these terms, the terms will evaluate to the same value. For example $$x \vee y \equiv y \vee x$$ because for all four possible assignments, the outcome is the same:

| $$x$$ | $$y$$ | $$x \vee y$$ | $$y \vee x$$ |
|-------|-------|--------------|--------------|
|0|0|0|0|
|0|1|1|1|
|1|0|1|1|
|1|1|1|1|

However, $$x \rightarrow y$$ is not equivalent to $$y \rightarrow x$$:

| $$x$$ | $$y$$ | $$x \rightarrow y$$ | $$y \rightarrow x$$ |
|-------|-------|---------------------|---------------------|
|0|0|1|1|
|0|1|1|0|
|1|0|0|1|
|1|1|1|1|

As a general rule, to show that two terms are _not_ equivalent, it is enough to find a single assignment to their variables that makes the outputs different. For example the assignment $$x = 1, y = 0$$ shows that $$x \rightarrow y$$ and $$y \rightarrow x$$ are not equivalent.

The following rules are useful enough to get names.

  * Identity: $$x \wedge 1 \equiv x$$ and $$x \vee 0 \equiv x$$.
  * Constant: $$x \wedge 0 \equiv 0$$ and $$x \vee 1 \equiv 1$$.
  * Either-or: $$x \wedge \neg x \equiv 0$$ and $$x \vee \neg x \equiv 1$$.
  * Commutative: $$x \wedge y \equiv y \wedge x$$ and $$x \vee y \equiv y \vee x$$.
  * Idempotent: $$x \wedge x \equiv x$$ and $$x \vee x \equiv x$$.
  * Double negation: $$\neg(\neg x) \equiv x$$.
  * Associative: $$x \wedge (y \wedge z) \equiv (x \wedge y) \wedge z$$ and the same for $$\vee$$.
  * De Morgan's laws: $$\neg (x \wedge y) \equiv (\neg x) \vee (\neg y)$$ and $$\neg(x \vee y) \equiv (\neg x) \wedge (\neg y)$$.
  * Distributive: $$x \vee (y \wedge z) \equiv (x \vee y) \wedge (x \vee z)$$ and $$x \wedge (y \vee z) \equiv (x \wedge y) \vee (x \wedge z)$$.
  * Absorption: $$x \wedge (x \vee y) \equiv x$$ and $$x \vee (x \wedge y) \equiv x$$.

The importance of these rules is that we can simplify terms or check if terms are equivalent even if they have lots of variables so writing out a full truth table would be impractical.
For example, in a term with 10 variables the truth table would already take $$2^{10} = 1024$$ rows; for 100 variables the truth table method would be completely impractical.
But you can use the rules above on any term in Boolean algebra, no matter how many variables.
